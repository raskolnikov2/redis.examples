package com.objectyl.redis;

import com.sun.istack.internal.Nullable;

import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import static com.objectyl.redis.RedisKeys.*;

/**
 * Wraps up common functionality for handling communication with Redis
 * Created by mehmet on 14/11/2016.
 */
public class RedisUtil {


	public static final Charset UTF8 = Charset.forName("UTF-8");
	public static final int MIN_ARCHIVE_POPULARITY = 5;


	public static final byte[] LISTINGS_PENDING_KEY = LISTINGS_PENDING.getBytes(UTF8);
	public static final byte[] LISTINGS_BY_START_DATE_KEY = BY_START_DATE.getBytes(UTF8);
	public static final byte[] LISTINGS_BY_END_DATE_KEY = BY_END_DATE.getBytes(UTF8);
	public static final byte[] LISTINGS_BY_CURRENT_AMOUNT_KEY = BY_CURRENT_AMOUNT.getBytes(UTF8);
	public static final byte[] LISTINGS_BY_POPULARITY_KEY = BY_POPULARITY.getBytes(UTF8);
	public static final byte[] LISTINGS_SHORT_IDS_KEY = SHORT_IDS.getBytes(UTF8);
	public static final byte[] CHARITIES_KEY = CHARITIES.getBytes(UTF8);
	public static final byte[] CHARITIES_INFOS_KEY = CHARITIES_INFOS.getBytes(UTF8);
	public static final byte[] CHARITIES_SHORT_IDS_KEY = CHARITIES_SHORT_IDS.getBytes(UTF8);
	public static final byte[] CATEGORIES_KEY = CATEGORIES.getBytes(UTF8);
	public static final byte[] QUEUE_KEY = QUEUE.getBytes(UTF8);
	public static final byte[] FAILED_QUEUE_KEY = FAILED_QUEUE.getBytes(UTF8);
	public static final byte[] PROCESSING_QUEUE_KEY = QUEUE_PROCESSING.getBytes(UTF8);


	private static final int MAX_SCORE_DIVISOR = 0x800000;


	@SuppressWarnings("unchecked")
	public static List<byte[]> getByteArrayList(Object o) {

		if (o instanceof List) {
			return (List<byte[]>) o;
		}
		return null;
	}


	public static String getLotKey(String uuid) {
		return LOT_PREFIX + uuid;
	}

	public static String getLotKey(UUID lotId) {
		return LOT_PREFIX + lotId;
	}

	public static byte[] getLotKeyBytes(UUID lotId) {
		return (LOT_PREFIX + lotId).getBytes(UTF8);
	}

	public static String getRetractedBidsKey(UUID lotId) {

		return LOT_PREFIX + lotId.toString() + RETRACTED_SUFFIX;
	}

	public static byte[] getRetractedBidsKeyBytes(UUID lotId) {

		return getRetractedBidsKey(lotId).getBytes(UTF8);
	}

	public static String getLotBidListKey(UUID lotId) {

		return LOT_PREFIX + lotId + BIDS_SUFFIX;
	}

	public static String getRaffleKey(String uuid) {
		return RAFFLE_PREFIX + uuid;
	}

	public static String getRaffleKey(UUID raffleId) {
		return RAFFLE_PREFIX + raffleId;
	}

	public static byte[] getRaffleKeyBytes(UUID raffleId) {
		return (RAFFLE_PREFIX + raffleId).getBytes(UTF8);
	}

	public static String getCanceledRafflePurchasesKey(UUID raffleId) {

		return RAFFLE_PREFIX + raffleId.toString() + CANCELLED_SUFFIX;
	}

	public static byte[] getCanceledRafflePurchasesKeyBytes(UUID raffleId) {

		return getCanceledRafflePurchasesKey(raffleId).getBytes(UTF8);
	}

	public static String getRafflePurchaseKey(UUID raffleId) {

		return RAFFLE_PREFIX + raffleId + PURCHASES_SUFFIX;
	}

	public static String getEventLotsKey(UUID eventId) {

		return EVENT_PREFIX + eventId.toString() + LOTS_SUFFIX;
	}

	public static byte[] getEventLotsKeyBytes(UUID eventId) {

		return getEventLotsKey(eventId).getBytes(UTF8);
	}

	public static String getEventRafflesKey(UUID eventId) {

		return EVENT_PREFIX + eventId.toString() + RAFFLES_SUFFIX;
	}

	public static byte[] getEventRafflesKeyBytes(UUID eventId) {

		return getEventRafflesKey(eventId).getBytes(UTF8);
	}

	public static String getGuestViewedKey(UUID guestId) {

		return GUEST_PREFIX + guestId.toString() + VIEWED_SUFFIX;
	}

	public static String getGuestCategoriesKey(UUID guestId) {

		return GUEST_PREFIX + guestId.toString() + CATEGORIES_SUFFIX;
	}

	public static String getGuestRecommendedKey(UUID guestId) {

		return GUEST_PREFIX + guestId.toString() + RECOMMENDED_SUFFIX;
	}


	public static byte[] getMaxReserveAgeArg(long offsetMillis) {
		return String.valueOf(offsetMillis).getBytes(UTF8);
	}


	public static String getListingsByCharityKey(String charityUuid) {

		return BY_CHARITY + charityUuid;
	}

	public static byte[] getListingsByCharityKeyBytes(String charityUuid) {

		return getListingsByCharityKey(charityUuid).getBytes(UTF8);
	}

	/**
	 * List of Redis keys that update_lot script operates on.
	 * This method must be kept in sync with update_lot.lua script
	 * {@see update_lot.lua}
	 */
	public static List<byte[]> getUpdateLotKeys(String lotKey) {

		List<byte[]> keys = new ArrayList<byte[]>();
		StringBuilder builder = new StringBuilder(64);
		builder.append(lotKey);
		keys.add(lotKey.getBytes(UTF8));
		keys.add(builder.append(BIDS_SUFFIX).toString().getBytes(UTF8));
		builder.setLength(lotKey.length());
		keys.add(builder.append(RETRACTED_SUFFIX).toString().getBytes(UTF8));
		keys.add(LISTINGS_SHORT_IDS_KEY);
		keys.add(CHARITIES_INFOS_KEY);
		return keys;
	}

	/**
	 * List of Redis keys that update_raffle script operates on.
	 * This method must be kept in sync with update_raffle.lua script
	 * {@see update_raffle.lua}
	 */
	public static List<byte[]> getUpdateRaffleKeys(String raffleKey) {

		List<byte[]> keys = new ArrayList<byte[]>();
		StringBuilder builder = new StringBuilder(64);
		builder.append(raffleKey);
		keys.add(raffleKey.getBytes(UTF8));
		keys.add(builder.append(PURCHASES_SUFFIX).toString().getBytes(UTF8));
		builder.setLength(raffleKey.length());
		keys.add(builder.append(CANCELLED_SUFFIX).toString().getBytes(UTF8));
		keys.add(LISTINGS_SHORT_IDS_KEY);
		keys.add(CHARITIES_INFOS_KEY);
		return keys;
	}

	public static String getListingsByCategoryKey(String mainCategory, @Nullable String subCategory) {

		StringBuilder builder = new StringBuilder(BY_CATEGORY);
		builder.append(mainCategory);
		if (subCategory != null)
			builder.append(":").append(subCategory);
		return builder.toString();
	}

	public static byte[] getListingsByCategoryKeyBytes(String mainCategory, @Nullable String subCategory) {

		return getListingsByCategoryKey(mainCategory, subCategory).getBytes(UTF8);
	}


	/**
	 * Removes accented characters from string turning it into plain ASCII
	 *
	 * @param string
	 * 		UTF-8 String to process
	 * @return processed String
	 */
	public static String transliterate(String string) {

		String normalised = Normalizer.normalize(string, Normalizer.Form.NFKD);
		StringBuilder buf = new StringBuilder(string.length());
		for (int i = 0; i < normalised.length(); ++i) {
			char ch = normalised.charAt(i);
			if (ch < 128)
				buf.append(ch);
		}
		return buf.toString();
	}

	/**
	 * Transliterates string and then takes up to first 3 characters and returns score for use with Redis.<br>
	 * <br>
	 * For "abc" you will get: <pre>'a'&lt;&lt;16 + 'b'&lt;&lt;8 + 'c'</pre><br>
	 * For "a" you will get: <pre>'a'&lt;&lt;16 + 0&lt;&lt;8 + 0</pre><br>
	 * <br>
	 * This allows to easily select ranges in Redis.
	 *
	 * @param string
	 * 		String to process
	 * @return numeric weight for given string
	 */
	public static int getScore(String string) {

		String asciiName = RedisUtil.transliterate(string).toLowerCase();
		int score = 0;
		for (int i = 0; i < 3; i++) {
			score = score << 8;
			if (i < asciiName.length()) {
				score += asciiName.charAt(i);
			}
		}
		return score;
	}

	/**
	 * Returns a fractional version of score calculated by {@link #getScore(String)} to fit in [0,1)
	 *
	 * @param string
	 * 		String to process
	 * @return fractional score
	 */
	public static double getFractionalScore(String string) {
		return (double) getScore(string) / MAX_SCORE_DIVISOR;
	}

	/**
	 * Returns list of indexes of elements for a shuffled list
	 *
	 * @param listSize
	 * 		size of list to shuffle
	 * @param offset
	 * 		starting element to return
	 * @param limit
	 * 		how many elements to return - if not enough elements left returns what's left
	 * @param seed
	 * 		random seed to use - specifying same seed allows for getting sections of same permutation in separate calls
	 * @return list of indexes of elements to retrieve from source list
	 */
	public static int[] getShuffledList(int listSize, int offset, int limit, long seed) {

		Random random = new Random(seed);
		if (offset >= listSize)
			return new int[0];
		if (offset + limit > listSize)
			limit = listSize - offset;
		if (limit < 1)
			return new int[0];
		int last = offset + limit;
		int replacedSize = (int) Math.round(listSize * 0.255) + 5;
		replacedSize = Math.min(offset + limit, replacedSize);
		Map<Integer, Integer> replaced = new HashMap<Integer, Integer>(replacedSize);
		int[] result = new int[limit];
		for (int i = 0; i < last; i++) {
			Integer tmp = replaced.get(i);
			int actualI = tmp == null ? i : tmp;
			int swap = random.nextInt(listSize - i) + i;
			tmp = replaced.get(swap);
			int actualSwap = tmp == null ? swap : tmp;
			if (i >= offset) {
				result[i - offset] = actualSwap;
			}
			if (swap > i) {
				replaced.put(swap, actualI);
			}
			replaced.remove(i);
		}
		return result;
	}

	public static <T> List<byte[]> toByteArrayList(List<T> idList) {
		return null;
	}

}
