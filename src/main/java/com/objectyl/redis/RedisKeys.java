package com.objectyl.redis;

/**
 * List of Redis keys used by application
 * Created by Swav Swiac on 09/08/2015.
 */
public class RedisKeys {

	public static final String LOT_TYPE = "lot";
	public static final String RAFFLE_TYPE = "raffle";
	public static final String EVENT_PREFIX = "event:";
	public static final String LOT_PREFIX = LOT_TYPE + ":";
	public static final String RAFFLE_PREFIX = RAFFLE_TYPE + ":";
	public static final String GUEST_PREFIX = "guest:";
	public static final String LISTINGS_PENDING = "listings:pending";
	public static final String BY_START_DATE = "listings:byStartDate";
	public static final String BY_END_DATE = "listings:byEndDate";
	public static final String BY_CURRENT_AMOUNT = "listings:byCurrentAmount";
	public static final String BY_POPULARITY = "listings:byPopularity";
	public static final String BY_POPULARITY_ARCHIVE = BY_POPULARITY + ":archive";
	public static final String BY_CHARITY = "listings:byCharity:"; // Redis format listings:byCharity:db297f60-401c-11e5-bd8e-0017f20b8b00
	public static final String BY_FEATURED = "listings:byFeatured";
	public static final String BY_CATEGORY = "listings:byCategory:";
	public static final String BY_TYPE_AUCTION = "listings:byType:auction";
	public static final String BY_TYPE_BUY_NOW = "listings:byType:buyNow";
	public static final String BY_TYPE_RAFFLE = "listings:byType:raffle";
	public static final String SHORT_IDS = "listings:shortIds";
	public static final String CHARITIES = "charities"; // sorted set weight - first letter
	public static final String CHARITIES_INFOS = "charities:infos";
	public static final String CHARITIES_SHORT_IDS = "charities:shortIds";
	public static final String CATEGORIES = "categories";
	public static final String LOTS_SUFFIX = ":lots";
	public static final String RAFFLES_SUFFIX = ":raffles";
	public static final String BIDS_SUFFIX = ":bids";
	public static final String RETRACTED_SUFFIX = ":retracted";
	public static final String PURCHASES_SUFFIX = ":purchases";
	public static final String CANCELLED_SUFFIX = ":cancelled";
	public static final String VIEWED_SUFFIX = ":viewed";
	public static final String CATEGORIES_SUFFIX = ":categories";
	public static final String RECOMMENDED_SUFFIX = ":recommended";
	public static final String PROCESSING_SUFFIX = ":processing";
	public static final String QUEUE = "queue";
	public static final String FAILED_QUEUE = "failedQueue";
	public static final String QUEUE_PROCESSING = QUEUE + PROCESSING_SUFFIX;
	public static final String TOKENS = "tokens";
}
